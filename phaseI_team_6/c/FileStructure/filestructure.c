#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <dirent.h>

void setupCourses(const char *filePath);

int main()
{
	setupCourses("data.txt");

	return 0;
}

void setupCourses(const char *filePath)
{
	FILE *file = fopen(filePath, "r");

	if (file == NULL)
	{
		perror("Error while opening the file.\n");
		exit(EXIT_FAILURE);
	}

	int null = open("/dev/null", O_RDONLY);

	if (!fork())
	{
		//The dup2() system call is similar to dup() but the basic difference between them is that instead of using the lowest-numbered unused file descriptor, it uses the descriptor number specified by the user.
		dup2(null, 2);
		close(null);
		execlp("mkdir", "mkdir", "adminSystem", NULL);
	}

	wait(NULL);

	int i = 0;
	char line[256] = "";
	char path[256] = "./adminSystem/";
	bool writing = false;

	while (fgets(line, 1024, file) != NULL)
	{
		
		/* Empty the path string to start making the new course directories.
		
		   We ignore new lines and spaces because we added some comments and new lines
		   in the data file to make the formatting of it more clear.
		*/
		if (line[0] == ' ' || !strcmp(line, "\n"))
		{
			strcpy(path, "./adminSystem/");
			i = 0;
			writing = false;
			
			continue;
		}
		
		if (line[strlen(line) - 1] == '\n')
			line[strlen(line) - 1] = '\0'; // \0 means end of string
		
		if (i < 5)
		{
			// Due date, bin, and src dirs
			if (i == 4)
			{
				// Due date format: DD-MM-YYYY
				char dueDatePath[256] = "";
				strcpy(dueDatePath, path);
				strcat(dueDatePath, "dueDate.txt");
				
				FILE *dueDateFile = fopen(dueDatePath, "a");
				fprintf(dueDateFile, "%s\n", line);
				
				fclose(dueDateFile);
				
				if (!fork())
				{
					dup2(null, 2);
					close(null);
					strcat(path, "/bin");
					execlp("mkdir", "mkdir", path, NULL);
				}
				
				if (!fork())
				{
					dup2(null, 2);
					close(null);
					strcat(path, "/src");
					execlp("mkdir", "mkdir", path, NULL);
				}
					
				wait(NULL);
				wait(NULL);
			
				i++;
				
				continue;
			}
			
			strcat(path, line);
			strcat(path, "/");
			
			// Create the course directory if it does not exist
			DIR *current_dir = opendir(path);
			if (!current_dir)
			{
				if (!fork())
				{
					dup2(null, 2);
					close(null);
					execlp("mkdir", "mkdir", path, NULL);
				}
				
				wait(NULL);
			} else
				closedir(current_dir);
			
			i++;
		}
		else
		{
			char membersPath[256] = "";
			strcpy(membersPath, path);
			
			// Create the members file one directory up (the team directory), because the current path contains the project or the homework directory
			strcat(membersPath, "../members.txt");

			if (!writing)
			{
				if (!fork())
				{
					dup2(null, 2);
					close(null);
					execlp("rm", "rm", membersPath, NULL);
				}

				wait(NULL);

				writing = true;
			}

			FILE *membersFile = fopen(membersPath, "a");
			fprintf(membersFile, "%s\n", line);

			fclose(membersFile);
		}
	}

	close(null);
	fclose(file);
}

