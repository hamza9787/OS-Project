#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#define __USE_XOPEN
#include <time.h>

int fileToString(const char *, char **);
int getAttemptGrade(char *);

int main()
{
    // Remove the reports file
    if (!fork())
    {
        int null = open("/dev/null", O_RDONLY);
        dup2(null, 2);
        execlp("rm", "rm", "./Report.txt", NULL);
    }

    wait(NULL);

    int pfds[2];
    pipe(pfds);

    if (!fork())
    {
        close(1);
        dup(pfds[1]);
        close(pfds[0]);
        close(pfds[1]);
        execlp("ls", "ls", "-ltr", "--time-style=+%s", "../../AutoGrader/Shell/feedback", NULL);
    }

    int out = open("./Report.txt", O_CREAT | O_RDWR, 0644);

    if (!fork())
    {
        close(0);
        dup(pfds[0]);
        close(pfds[0]);
        close(pfds[1]);
        dup2(out, 1);
        close(out);
        execlp("awk", "awk", "(NR>1){print $6 \"\\n\" $7;}", NULL);
    }

    close(pfds[0]);
    close(pfds[1]);
    close(out);
    wait(NULL);
    wait(NULL);

    char *reports[1024] = {""};
    char dueDate[16] = "19-02-2021";
    char lastFile[64] = "";

    int lines = fileToString("./Report.txt", reports);
    int succesfulAttempts = 0;
    int lastGrade = 0;

    struct tm time;
    strptime(dueDate, "%d-%m-%Y", &time);

    // Deadline timestamp
    const long int deadLine = mktime(&time) + 3600 * 24 - 1;

    FILE *reportFile = fopen("Report.txt", "w");

    if (lines > 0)
        fprintf(reportFile, "Attempts Details:\n\nDate\t\t\tAttempts Feedback\tGrade\n\n");
    else
        fprintf(reportFile, "No submissions were recieved");

    for (int i = 0; i < lines; i += 2)
    {
        const long int submissionDate = atol(reports[i]);
        int grade = getAttemptGrade(reports[i + 1]);

        if (submissionDate <= deadLine)
        {
            lastGrade = grade;
            strncpy(lastFile, reports[i + 1], sizeof(lastFile));
        }

        if (grade > 0)
            succesfulAttempts++;

        struct tm date;
        char fileDate[32] = "";
        date = *localtime(&submissionDate);

        strftime(fileDate, sizeof(fileDate), "%d-%m-%Y %H:%M", &date);

        fprintf(reportFile, "%s\t%s\t\t%d\n", fileDate, reports[i + 1], grade);
    }

    if (lines > 0)
        fprintf(reportFile, "\n\nTotal Number of attempts: %d\n\n%d out of %d attempts were solved correctly!\n\nProject Due date: %s\n\nLast submission befor due date: %s\n\nFinal grade: %d/1", lines / 2, succesfulAttempts, lines / 2, dueDate, lastFile, lastGrade);

    fclose(reportFile);

    // Prevent memory leaks
    while (lines > 0)
        free(reports[--lines]);

    return 0;
}

int fileToString(const char *filePath, char **dest)
{
    FILE *file = fopen(filePath, "r");
    if (file == NULL)
    {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    int i = 0;
    char line[1024] = "";

    while (fgets(line, 1024, file) != NULL)
    {
        dest[i++] = malloc(1024);

        if (line[strlen(line) - 1] == '\n')
            line[strlen(line) - 1] = '\0';

        strcpy(dest[i - 1], line);
    }

    fclose(file);

    return i;
}

int getAttemptGrade(char *fileName)
{
    int grade = 0;
    char filePath[128] = "../../AutoGrader/Shell/feedback/";
    strcat(filePath, fileName);

    FILE *file = fopen(filePath, "r");
    if (file == NULL)
    {
        perror("Error while opening the feedback file.\n");
        exit(EXIT_FAILURE);
    }

    char line[512] = "";

    while (fgets(line, 512, file) != NULL)
        if (strstr(line, "Grade: 1") != NULL)
            grade = 1;

    fclose(file);

    return grade;
}