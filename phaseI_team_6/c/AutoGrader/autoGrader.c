#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
// #include <dirent.h> is a struct discibes the file
char *getNextFeedbackNo();

void main()
{
    // Need to be selected by last modification
    char FILE_PATH[256] = "./student_src/program.c";
    char TO_RUN_FILE[64] = "./student_src/compilation/compiled.bin";
    char sample_output[2048] = "";
    char student_output[2048] = "";
    char student_error[2048] = "";
    char *feedback_file = getNextFeedbackNo();
    int are_same_outputs = 0;

    int student_error_file = open("./student_src/compilation/student_error", O_CREAT | O_RDWR, 0644); // to read it later on inshaallah
    int null_error = open("/dev/null", O_WRONLY);
    if (!fork())
    {
        // Removing the old compiled/output files is important,
        // consider the next code was not running so it will take the previous compiled file,
        // so that it should not be there when program starts.
        dup2(null_error, 2);
        execlp("rm", "rm", "./student_src/compilation/compiled.bin", "./student_src/compilation/output_result.txt", NULL);
    }
    else
    {
        wait(NULL);
        if (!fork())
        {
            dup2(student_error_file, 2);
            execlp("gcc", "gcc", "-o", TO_RUN_FILE, FILE_PATH, NULL);
        }
        else
        {
            // Starting from here is geting the out put of both student and sample,
            // to comapre them and generate the result.

            wait(NULL);
            // The open() system call opens the file specified by pathname.
            // If the specified file does not exist, O_CREAT will creat it
            // O_RDWR is request opening the read/write mode
            // The return value of open() is a file descriptor
            int output_result = open("./student_src/compilation/output_result.txt", O_CREAT | O_RDWR, 0644);
            if (!fork())
            {
                // run for the compiled file
                dup2(output_result, 1);
                execlp("././student_src/compilation/compiled.bin", "././student_src/compilation/compiled.bin", NULL);
                // Still need to validate if it was not compiled sucsesfully, so how to run file DNE !
            }
            else
            {
                wait(NULL);

                // Read expected result into a variable
                FILE *sample_output_file = fopen("./sample_output.txt", "r");
                if (sample_output_file == NULL)
                {
                    perror("Error while opening the samlpe file.\n");
                    exit(EXIT_FAILURE);
                }
                char ch1;
                int i = 0;
                while ((ch1 = fgetc(sample_output_file)) != EOF)
                {
                    sample_output[i] = ch1;
                    i++;
                }

                // Read student result into a variable
                FILE *student_output_file = fopen("./student_src/compilation/output_result.txt", "r");
                if (student_output_file == NULL)
                {
                    perror("Error while opening the student file, seems eas no output!\n");
                    exit(EXIT_FAILURE);
                }
                char ch2;
                int j = 0;
                while ((ch2 = fgetc(student_output_file)) != EOF)
                {
                    student_output[j] = ch2;
                    j++;
                }

                // To solve problem: if the last line was (\n) remove it 
                if (i > 0 && sample_output[i - 1] == '\n')
                    sample_output[i - 1] = '\0';

                // Generate the Feedback file
                FILE *file = fopen(feedback_file, "a");
                if (file == NULL)
                {
                    perror("Unable to open the feedback file!");
                    exit(EXIT_FAILURE);
                }

                // Check if same outputs
                // strcmp() return 0 if both strings are identical (equal)
                if (!strcmp(student_output, ""))
                {
                    // means some thing went wrong while compiling, so i will get the compilation error
                    FILE *error_file = fopen("./student_src/compilation/student_error", "r");
                    if (error_file == NULL)
                    {
                        perror("Error while opening the student error file.\n");
                        exit(EXIT_FAILURE);
                    }
                    char ch;
                    int i = 0;
                    while ((ch = fgetc(error_file)) != EOF)
                    {
                        student_error[i] = ch;
                        i++;
                    }
                    fprintf(file, "Your Output:\n\n[Compilation Error]\n%s\n\nExpected Output:\n%s\n\nFeedback: \nTry Again!\n\nGrade: 0", student_error, sample_output);
                }
                else if (strcmp(student_output, sample_output) == 0)
                {
                    fprintf(file, "Your Output:\n%s\n\nExpected Output:\n%s", student_output, sample_output);
                    fprintf(file, "\n\nFeedback: \nWell Done, Correct answer!\n\nGrade: 1");
                }
                else
                {
                    fprintf(file, "Your Output:\n%s\n\nExpected Output:\n%s", student_output, sample_output);
                    fprintf(file, "\n\nFeedback: \nTry Again!\n\nGrade: 0");
                }

                fclose(file);

                // Change feedback file permisiions to be visible for groups only (read, and write for the teams, not for others) (as required)
                if (!fork())
                {
                    dup2(null_error, 2);
                    execlp("chmod", "chmod", "660", feedback_file, NULL);
                }
            }
        }
    }
}
// Generate the name of the next Feedback file => (echo \"$(ls -la | grep Feedback | wc -l)+1\" | bc)
char *getNextFeedbackNo()
{
    struct dirent **files;

    // scandir return the number of directory entries in the path directory,
    // and fileListTemp will point to an allocated array of pointers to allocated struct
    // dirent blobs each of which has a d_name member which points to the null-terminated name of a file/directory.
    int i = scandir("./feedback", &files, NULL, alphasort);
    if (i == -1)
    {
        printf("scandir error!");
        return NULL;
    }
    else if (i <= 2)
        return "feedback/Feedback_1.txt";

    int numberOfMatches = 1;
    while (i--)
    {
        // d_type gives the type (dir or file)
        // DT_REG = regular file
        if (files[i]->d_type == DT_REG)
            // d_name make sure it is a feedback file
            if (!strncmp(files[i]->d_name, "Feedback", strlen("Feedback")))
                numberOfMatches++;

        free(files[i]);
    }

    free(files);

    static char feedback_file[256];
    // Feedback folder must be there TODO (check if not there, create it) !
    snprintf(feedback_file, sizeof feedback_file, "feedback/Feedback_%d.txt", numberOfMatches);

    return feedback_file;
}
