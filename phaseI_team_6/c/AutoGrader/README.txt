Instructions and Assumptions:

1- The sample output should placed be in the (sample_output.txt) file.
2- The code file to be graded should be placed in the (student_src) folder with the name (program.c).
3- The auto-generated feedback file will be in the (feedback) folder.
