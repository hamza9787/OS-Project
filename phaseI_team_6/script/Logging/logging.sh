#!/bin/bash

# Logging File Name
logging_file='Report.txt'
path="../AutoGrader/feedback/"

# Get files list ordered by date
feedback_files=$(ls -tr -1 $path)

last_feedback_file=""
last_feedback_filename=""

# Get total number of feedback files
feedback_files_number=$(ls -1 $path | wc -l)

if [[ $feedback_files_number == 0 ]] ; then
	echo "No submissions were recieved" > "$logging_file"
	exit;
fi
echo -e "Attempts Details:\n\n\t  Date\t\t   Attempts Feedback  Grade\n" > "$logging_file"

deadline=$(echo $(date -d "2021-03-3" "+%s")+3600*24-1 | bc)

# Loop through the lines from $feedback_files
while read -r line; do
	fileDate=$(ls $path$line -l --time-style='+%Y-%m-%d %H:%M' | cut -d " " -f 6-7)
	fileGrade=$(cat $path$line | grep "Grade:" | cut -d " " -f 2)
	
	printf '%s\t%s\t\t%s\n' "${fileDate}" $line "$fileGrade" >> "$logging_file"
	
	fileDate=$(date -d "$fileDate" "+%s")
	if [[ $fileDate < $deadline ]] || [[ $fileDate == $deadline ]] ; then
		last_feedback_filename=$line
		last_feedback_file="$path$line"
	fi
done <<< $feedback_files

# Print number of attempts to logging files
echo -e "\n\n\nTotal Number of attempts: $feedback_files_number" >> "$logging_file"

# Number of times (Grade: 1) was written
num_of_corrects=$(cat ../AutoGrader/feedback/Feedback_* | grep "Grade: 1" | wc -l)

# Print number of correct out of total
echo -e "\n$num_of_corrects out of $feedback_files_number attempts were solved correctly!\n\nLast submission file: $last_feedback_filename" >> "$logging_file"

# Get the final grade from last feedback file
last_grade=$(cat $last_feedback_file | grep "Grade: 1" | wc -l)

# Print last feedback (attempt) grade
echo -e "\nLast submission grade: $last_grade/1" >> "$logging_file"
