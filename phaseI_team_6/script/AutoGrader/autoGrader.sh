#!/bin/bash

# Takse the (project, HW,..etc) src foledr path as the first parameter
PROJECT_SRC_PATH="$1"

# Which exact (project, HW,..etc) src file you want to grade
FILE_NAME=$2

# bin folder path for the project
BIN_Folder_PATH="$PROJECT_SRC_PATH/../bin/"

# Name of result of compilation
TO_RUN="compiled.bin"

# Delete the exsisting compiled binary file
rm "$BIN_Folder_PATH/$TO_RUN" 2>/dev/null

# Compile the src file and if compilation error store it is file student_error
gcc -o "$BIN_Folder_PATH/$TO_RUN" "$PROJECT_SRC_PATH/$FILE_NAME" 2>"$BIN_Folder_PATH/student_error"

# Execute and hold the result in a variable
student_output=$("$BIN_Folder_PATH/$TO_RUN" 2>/dev/null)

# There is no out put, so read what was the error
if [[ "$student_output" = "" ]]; then
	error=$(cat "$BIN_Folder_PATH"/student_error)
fi

# Read the given sample output into a variable
sample_output=$(cat "$PROJECT_SRC_PATH/../sample_output.txt")

# Creates the feedback folder if it doesn't exist
[ ! -d "$PROJECT_SRC_PATH/../Feedbacks" ] && mkdir "$PROJECT_SRC_PATH/../Feedbacks"

# New feedback file name with its number
feedback_file="$PROJECT_SRC_PATH/../Feedbacks/"$FILE_NAME"_Feedback.txt"

# Print the student output together with sample output in the feedback file
# then Print the feedback depending on the student's output
# then print the grade
echo -e "Note: This Feedback file is for "$FILE_NAME"\n" >"$feedback_file"
if [[ "$student_output" = "$sample_output" ]]; then
	echo -e "Your Output:\n$student_output\n\nExpected Output:\n$sample_output" >>"$feedback_file"
	echo -e "\nFeedback: \nWell Done, it's a correct answer!" >>"$feedback_file"
	echo -e "\nGrade: 1" >>"$feedback_file"
elif [[ "$student_output" = "" ]]; then
	echo -e "Your Output:\n\nCompilation Error:\n\n$error\n\nExpected Output:\n$sample_output" >>"$feedback_file"
	echo -e "\nFeedback: \nTry Again!" >>"$feedback_file"
	echo -e "\nGrade: 0" >>"$feedback_file"
else
	echo -e "Your Output:\n$student_output\n\nExpected Output:\n$sample_output" >>"$feedback_file"
	echo -e "\nFeedback: \nTry Again!" >>"$feedback_file"
	echo -e "\nGrade: 0" >>"$feedback_file"
fi

# Change the mode of the feedback file to be visible for groups only (read, and write for the teams, not for others) (as required)
chmod 660 "feedback/$feedback_file" 2>/dev/null
