Instructions and Assumptions:

1- The sample output should placed be in the (sample_output.txt) file.
2- The path of the code file to be graded should be passed as first parametr to this script
   if more than one file exists, then the last modified one will be taken.
3- The auto-generated feedback file will be in the (Feedback) folder in the spicified path.
