1- Place this whole folder (OS-Project) in your eclipse-workspace.

2- Import/open from it the project (Server Side) to run the server.
3- Befor that You have to fix the gson.jar path in the project (Server Side) 
   Go for properties -> Java Build Path -> Libraries -> Classpath -> remove the old one -> click (External JARs) 
   and add the new one provided in (OS-Project) folder.

4- Import/open also project (Client Side) as a different project, to run the UI and consume the 
   services provided by the running server.
5- You have to be already setup the JavaFx staff, if not (javafx-sdk-13.0.1) might help you while doing that.

6- Regarding (phaseI_team_6) folder, this is where we find (File Structure), and (AutoGrader) to use them in 
   the server services.
7- We used the shell version for the (AutoGrader), and C version for (File Structure).
   -BTW they had some updates-
