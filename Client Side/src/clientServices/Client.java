package clientServices;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;

import model.User;
import utilities.UserHolder;

public abstract class Client {
	private static Socket server = null;
	private static String host = "192.168.100.4";

	// All Services will sent the User Cookie in the head of any request
	public static String loginService(String userName, String password) {
		Formatter toServer = null;
		Scanner fromServer = null;
		try {
			server = new Socket(host, 4000);
			fromServer = new Scanner(server.getInputStream());
			toServer = new Formatter(server.getOutputStream());

			// send to server
			toServer.format("%s\n", userName);
			toServer.flush();
			toServer.format("%s\n", password);
			toServer.flush();

			String response = fromServer.nextLine();
			String cookie = "";
			if (response.equals("{")) { // if authentication was successful, then the response will be the user JSON
										// object, so I have to loop for all lines to take the whole JSON object.
				while (true) {
					String line = fromServer.nextLine();
					if (line.equals("}")) {
						response += line;
						cookie = fromServer.nextLine();
						break;
					}
					response += line;
				}

				Gson gson = new Gson();
				UserHolder.getInstance().setCurrentUser(gson.fromJson(response, User.class));
				UserHolder.getInstance().setCookie(cookie);
			}
			System.out.println("Response: " + cookie);
			return response;
		} catch (IOException e) {
			e.printStackTrace();
			return e.toString();
		} finally {
			try {
				if (toServer != null)
					toServer.close();
				if (fromServer != null)
					fromServer.close();
				if (server != null)
					server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static ArrayList<String> getPathChildes(String path) {
		Formatter toServer = null;
		Scanner fromServer = null;
		ArrayList<String> childs = new ArrayList<String>();
		try {
			server = new Socket(host, 4002);
			fromServer = new Scanner(server.getInputStream());
			toServer = new Formatter(server.getOutputStream());

			toServer.format("%s\n", UserHolder.getInstance().getCookie());
			toServer.flush();
			toServer.format("%s\n", path);
			toServer.flush();

			String responseLine = "";
			while (true) {
				responseLine = fromServer.nextLine();
				if (responseLine.equals("") || responseLine.isBlank() || responseLine.isEmpty())
					break;
				else
					childs.add(responseLine);
				System.out.println("Response: " + responseLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (server != null)
					server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return childs;
	}

	public static void uploadFiles(List<File> uploadFiles, int indexOfCourse, String CourseAssignment) {
		try {
			for (File uploadFile : uploadFiles) {
				server = new Socket(host, 4001);
				OutputStream toNet = server.getOutputStream();
				InputStream fileStream = new FileInputStream(uploadFile);
				// append to head of file content [ 1- file name, 2- course index(one char only
				// for the index), 2- which assignment in this course ]
				toNet.write((UserHolder.getInstance().getCookie() + "|" + uploadFile.getName() + "|" + indexOfCourse
						+ "|" + CourseAssignment + "/").getBytes());
				toNet.flush();
				byte[] fileBufferBytes = fileStream.readAllBytes();
				toNet.write(fileBufferBytes);
				toNet.flush();
				server.close();
				if (fileStream != null)
					fileStream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (server != null)
					server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String doenloadFile(String path, String fileName) {
		Formatter toServer = null;
		InputStream fromNet = null;
		String fileContentAsString = "";
		try {
			server = new Socket(host, 4004);
			fromNet = server.getInputStream();
			toServer = new Formatter(server.getOutputStream());

			toServer.format("%s\n", UserHolder.getInstance().getCookie());
			toServer.flush();
			// Send file name to server
			toServer.format("%s\n", path);
			toServer.flush();

			byte[] fileContent = fromNet.readAllBytes();

			// this content as string will be displayed when user only click open file (not
			// for download)
			fileContentAsString = new String(fileContent);

			String destination = "./downloads/";
			File destDir = new File(destination);
			if (!destDir.exists())
				destDir.mkdir();
			OutputStream fileStream = new FileOutputStream(destination + fileName);
			fileStream.write(fileContent);

			if (fileStream != null)
				fileStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (toServer != null)
					toServer.close();
				if (fromNet != null)
					fromNet.close();
				if (server != null)
					server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return fileContentAsString;
	}

	public static String resetPassword(String email, String password1, String password2) {
		Formatter toServer = null;
		Scanner fromServer = null;
		String response = "";
		try {
			server = new Socket(host, 4003);
			fromServer = new Scanner(server.getInputStream());
			toServer = new Formatter(server.getOutputStream());

			toServer.format("%s\n", UserHolder.getInstance().getCookie());
			toServer.flush();
			toServer.format("%s\n", email);
			toServer.flush();
			toServer.format("%s\n", password1);
			toServer.flush();
			toServer.format("%s\n", password2);
			toServer.flush();

			response = fromServer.nextLine();
			System.out.println("Response: " + response);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (toServer != null)
					toServer.close();
				if (fromServer != null)
					fromServer.close();
				if (server != null)
					server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return response;
	}
}
