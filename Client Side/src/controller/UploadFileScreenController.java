package controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import clientServices.Client;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Course;
import model.User;
import utilities.AppUtilities;
import utilities.UserHolder;

public class UploadFileScreenController {
	@FXML
	private ComboBox<Course> coursesComboBox;

	@FXML
	private Button submitBTN;

	@FXML
	private Button browesBTN;

	@FXML
	private Label welcomeLB;

	@FXML
	private Label filesToUploadLB;

	@FXML
	private Label userInfoLB;

	@FXML
	private ComboBox<String> coursefilesComboBox;

	List<File> uploadFiles = new ArrayList<File>();

	@FXML
	public void initialize() {
		// init user courses-team
		if (UserHolder.getInstance().getCurrentUser() != null) {
			User user = UserHolder.getInstance().getCurrentUser();
			String userMessage = "User: " + user.getUserName() + "\tEmail: "
					+ UserHolder.getInstance().getCurrentUser().getEmail();
			userInfoLB.setText(userMessage);
			// display user courses
			coursesComboBox.setItems(FXCollections.observableArrayList(user.getCourses()));
			coursesComboBox.getSelectionModel().selectFirst();
		}
		// init folders inside course team
		int indexOfCourse = coursesComboBox.getSelectionModel().getSelectedIndex();
		Course c = UserHolder.getInstance().getCurrentUser().getCourses().get(indexOfCourse);
		String path = c.course + "/" + c.instructor + "/" + c.team + "/";
		ArrayList<String> files = Client.getPathChildes("0" + path);
		coursefilesComboBox.setItems(FXCollections.observableArrayList(files));
		coursefilesComboBox.getSelectionModel().selectFirst();
	}

	@FXML
	void browesBTN(ActionEvent event) throws IOException {
		FileChooser fileChooser = new FileChooser();
		uploadFiles = fileChooser.showOpenMultipleDialog(new Stage());
		if (uploadFiles != null && !uploadFiles.isEmpty()) {
			String files = "";
			for (File f : uploadFiles) {
				files += f.getName() + "\n";
			}
			filesToUploadLB.setText(files);
		}
	}

	@FXML
	void coursesComboBox(ActionEvent event) {
		// update folders inside course team based on user choice (click)
		int indexOfCourse = coursesComboBox.getSelectionModel().getSelectedIndex();
		Course c = UserHolder.getInstance().getCurrentUser().getCourses().get(indexOfCourse);
		String path = c.course + "/" + c.instructor + "/" + c.team + "/";
		ArrayList<String> files = Client.getPathChildes("0" + path);

		coursefilesComboBox.setItems(FXCollections.observableArrayList(files));
		coursefilesComboBox.getSelectionModel().selectFirst();
	}

	@FXML
	void coursefilesComboBox(ActionEvent event) {

	}

	@FXML
	void submitBTN(ActionEvent event) throws IOException {
		if (uploadFiles != null && !uploadFiles.isEmpty()) {
			String files = "";
			for (File f : uploadFiles) {
				files += f.getName() + "\n";
			}
			Optional<ButtonType> result = AppUtilities.getInstance().showAlert(AlertType.CONFIRMATION, "Confiramtion",
					"Are you sure of uploading files:\n" + files);
			if (result.get() == ButtonType.OK) {
				int indexOfCourse = coursesComboBox.getSelectionModel().getSelectedIndex();
				String CourseAssignment = coursefilesComboBox.getValue();
				if (CourseAssignment.contains("Passed due")) {
					AppUtilities.getInstance().showAlert(AlertType.ERROR, "Due Date",
							"Sorry, This course has passed due!");
				} else {
					System.out.println(CourseAssignment);
					int indexOfDelimiter = CourseAssignment.indexOf('|');
					CourseAssignment = CourseAssignment.substring(0, indexOfDelimiter).trim();
					Client.uploadFiles(uploadFiles, indexOfCourse, CourseAssignment);
					filesToUploadLB.setText("");
					uploadFiles = new ArrayList<File>();
					// Close the window
					Stage stage = (Stage) submitBTN.getScene().getWindow();
					stage.close();
				}
			} else {
				uploadFiles = new ArrayList<File>();
				filesToUploadLB.setText("");
			}
		} else {
			AppUtilities.getInstance().showAlert(AlertType.ERROR, "Uploading Failed",
					"Please choose at least one file!");
		}
	}
}
