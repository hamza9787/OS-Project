package controller;

import app.Navigate_Download;
import app.UploadFile;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.User;
import utilities.UserHolder;

public class ServicesMenuController {
	@FXML
	private Button uploadFilesBTN;

	@FXML
	private Button navigationBTN;

	@FXML
	private Label userInfoLB;

	@FXML
	public void initialize() {
		if (UserHolder.getInstance().getCurrentUser() != null) {
			User user = UserHolder.getInstance().getCurrentUser();
			String userMessage = "User: " + user.getUserName() + "\tEmail: "
					+ UserHolder.getInstance().getCurrentUser().getEmail();
			userInfoLB.setText(userMessage);
		}
	}

	@FXML
	void navigationBTN(ActionEvent event) throws Exception {
		// Close the window
		Stage stage = (Stage) navigationBTN.getScene().getWindow();
		stage.hide();
		// Start new window
		Navigate_Download navigate_Download = new Navigate_Download();
		Stage s = new Stage();
		navigate_Download.start(s);
		s.hide();
		s.showAndWait();
		stage.show();
	}

	@FXML
	void uploadFilesBTN(ActionEvent event) throws Exception {
		// Close the window
		Stage stage = (Stage) uploadFilesBTN.getScene().getWindow();
		stage.hide();
		// Start new window
		UploadFile uploadFile = new UploadFile();
		Stage s = new Stage();
		uploadFile.start(s);
		s.hide();
		s.showAndWait();
		stage.show();
	}

}
