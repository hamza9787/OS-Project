package controller;

import clientServices.Client;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import utilities.AppUtilities;

public class ResetPasswordScreenController {

	@FXML
	private TextField emailTF;

	@FXML
	private TextField newPassword1TF;

	@FXML
	private TextField newPassword2TF;

	@FXML
	private Button resetBTN;

	@FXML
	void resetBTN(ActionEvent event) {
		String email, password1, password2;

		email = emailTF.getText().trim();
		password1 = newPassword1TF.getText();
		password2 = newPassword2TF.getText();

		if (email.isEmpty() || email.equals("") || password1.isEmpty() || password1.equals("") || password2.isEmpty()
				|| password2.equals(""))
			AppUtilities.getInstance().showAlert(AlertType.ERROR, "Error", "Please fill all requirements");
		else {
			if (!password1.equals(password2))
				AppUtilities.getInstance().showAlert(AlertType.ERROR, "Error", "Passwords do not match");
			else {
				String response = Client.resetPassword(email, password1, password2);
				AppUtilities.getInstance().showAlert(AlertType.INFORMATION, "Response", response);
				// Close the window
				Stage stage = (Stage) resetBTN.getScene().getWindow();
				stage.close();
			}
		}
	}

}
