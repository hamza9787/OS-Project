package controller;

import app.ResetPassword;
import app.ServiceMenu;
import clientServices.Client;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import utilities.AppUtilities;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginScreenController {

	@FXML
	private Button loginBTN;

	@FXML
	private TextField userNameTF;

	@FXML
	private PasswordField passwordTF;

	@FXML
	private Button forgotPassBTN;

	@FXML
	void loginBTN(ActionEvent event) throws Exception {
		String userName = userNameTF.getText().trim();
		String password = passwordTF.getText().trim();
		if (userName.isBlank() || userName.isEmpty() || password.isBlank() || password.isEmpty())
			AppUtilities.getInstance().showAlert(AlertType.ERROR, "Empty Fields",
					"Please enter both Username and Password");
		else {
			String response = Client.loginService(userName, password);
			if (response.charAt(0) == '{') {
				AppUtilities.getInstance().showAlert(AlertType.CONFIRMATION, "Response", "Welcome");
				// Close the window
				Stage stage = (Stage) loginBTN.getScene().getWindow();
				stage.close();
				// open main menu window
				ServiceMenu serviceMenu = new ServiceMenu();
				serviceMenu.start(new Stage());
			} else {
				AppUtilities.getInstance().showAlert(AlertType.ERROR, "Response", response);
			}
		}
	}

	@FXML
	void forgotPassBTN(ActionEvent event) throws Exception {
		// Start new window
		ResetPassword resetPassword = new ResetPassword();
		resetPassword.start(new Stage());
	}

}
