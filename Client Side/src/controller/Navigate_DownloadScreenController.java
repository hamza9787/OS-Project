package controller;

import java.util.ArrayList;

import clientServices.Client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import model.Course;
import model.User;
import utilities.AppUtilities;
import utilities.UserHolder;

public class Navigate_DownloadScreenController {

	@FXML
	private ListView<Course> courseList;

	@FXML
	private ListView<String> dirContentList;

	@FXML
	private Button openBTN;

	@FXML
	private Button downloadBTN;

	@FXML
	private Button backBTN;

	@FXML
	private Label bathLB;

	@FXML
	private Label userInfoLB;

	private ArrayList<String> currentPath = new ArrayList<>();

	@FXML
	public void initialize() {
		// init user courses List
		if (UserHolder.getInstance().getCurrentUser() != null) {
			User user = UserHolder.getInstance().getCurrentUser();
			String userMessage = "User: " + user.getUserName() + "\tEmail: "
					+ UserHolder.getInstance().getCurrentUser().getEmail();
			userInfoLB.setText(userMessage);
			// display user courses
			courseList.setItems(FXCollections.observableArrayList(user.getCourses()));
			courseList.getSelectionModel().selectFirst();

			// display what inside the course's team folder
			int indexOfCourse = courseList.getSelectionModel().getSelectedIndex();
			Course c = UserHolder.getInstance().getCurrentUser().getCourses().get(indexOfCourse);
			String path = c.course + "/" + c.instructor + "/" + c.team + "/";

			currentPath.add(path);
			ArrayList<String> files = Client.getPathChildes("1" + currentPath.get(currentPath.size() - 1));
			dirContentList.setItems((ObservableList<String>) FXCollections.observableArrayList(files));

			bathLB.setText(currentPath.get(currentPath.size() - 1));

			courseList.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent click) {
					if (click.getClickCount() == 2) {

						int selectedCourseIndex = courseList.getSelectionModel().getSelectedIndex();
						Course c = UserHolder.getInstance().getCurrentUser().getCourses().get(selectedCourseIndex);
						String path = c.course + "/" + c.instructor + "/" + c.team + "/";

						currentPath.clear();
						currentPath.add(path);
						ArrayList<String> files = Client.getPathChildes("1" + currentPath.get(currentPath.size() - 1));
						dirContentList.setItems((ObservableList<String>) FXCollections.observableArrayList(files));

						bathLB.setText(currentPath.get(currentPath.size() - 1));
					}
				}
			});

			dirContentList.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent click) {
					if (click.getClickCount() == 2) {
						String selectedfileOrDirName = dirContentList.getSelectionModel().getSelectedItem();
						if (selectedfileOrDirName.subSequence(0, 1).equals("/")) { // A directory was selected
							currentPath.add(currentPath.get(currentPath.size() - 1) + "/" + selectedfileOrDirName);
							ArrayList<String> files = Client
									.getPathChildes("1" + currentPath.get(currentPath.size() - 1));
							dirContentList.setItems((ObservableList<String>) FXCollections.observableArrayList(files));

							bathLB.setText(currentPath.get(currentPath.size() - 1));
						} else { // file is clicked, so open it
							String fileName = dirContentList.getSelectionModel().getSelectedItem();
							String fileContent = Client
									.doenloadFile(currentPath.get(currentPath.size() - 1) + "/" + fileName, fileName);
							Alert alert = new Alert(AlertType.INFORMATION, fileContent, ButtonType.OK);
							alert.setTitle(fileName);
							alert.setHeaderText(fileName);
							alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
							alert.show();

						}
					}
				}
			});
		}
	}

	@FXML
	void backBTN(ActionEvent event) {
		if (currentPath.size() > 1) {
			currentPath.remove((currentPath.get(currentPath.size() - 1)));
			ArrayList<String> files = Client.getPathChildes("1" + currentPath.get(currentPath.size() - 1));
			dirContentList.setItems((ObservableList<String>) FXCollections.observableArrayList(files));
			bathLB.setText(currentPath.get(currentPath.size() - 1));
		}
	}

	@FXML
	void openBTN(ActionEvent event) {
		String fileName = dirContentList.getSelectionModel().getSelectedItem();
		if (fileName.contains("/"))
			AppUtilities.getInstance().showAlert(AlertType.ERROR, "Not a file",
					"You are trying to download a directory!");
		else {
			System.out.println(currentPath.get(currentPath.size() - 1));
			String fileContent = Client.doenloadFile(currentPath.get(currentPath.size() - 1) + "/" + fileName,
					fileName);
			Alert alert = new Alert(AlertType.INFORMATION, fileContent, ButtonType.OK);
			alert.setTitle(fileName);
			alert.setHeaderText(fileName);
			alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
			alert.show();
		}
	}

	@FXML
	void downloadBTN(ActionEvent event) {
		String fileName = dirContentList.getSelectionModel().getSelectedItem();
		if (fileName.contains("/"))
			AppUtilities.getInstance().showAlert(AlertType.ERROR, "Not a file",
					"You are trying to download a directory!");
		else {
			String path = currentPath.get(currentPath.size() - 1) + "/" + fileName;
			Client.doenloadFile(path, fileName);
		}
	}
}
