package model;

public class Course {

	public String instructor;
	public String course;
	public String team;

	@Override
	public String toString() {
		return course + " - " + instructor + " - " + team;
	}
}
