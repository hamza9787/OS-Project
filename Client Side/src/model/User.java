package model;

import java.util.ArrayList;

public class User {
	private String userName;
	private String password;
	private String email;
	private int attempts = 3;
	private ArrayList<Course> courses;

	public User(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	public User(String userName, String password, String email) {
		super();
		this.userName = userName;
		this.password = password;
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String passwor) {
		this.password = passwor;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAttempts() {
		return attempts;
	}

	public void reSetAttempts() {
		attempts = 3;
	}

	public void decrementAttempts() {
		if (attempts != 0)
			attempts--;
	}

	public ArrayList<Course> getCourses() {
		return courses;
	}

	public void setCourses(ArrayList<Course> courses) {
		this.courses = courses;
	}

	@Override
	public String toString() {
		return "[userName=" + userName + ", password=" + password + ", email=" + email + ", attempts=" + attempts
				+ ", courses=" + courses + "]";
	}

}
