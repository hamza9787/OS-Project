package utilities;

import model.User;

public class UserHolder {

	private static UserHolder single_instance = null;
	private User CurrentUser;
	private String cookie;

	// private constructor restricted to this class itself
	private UserHolder() {
	}

	// static method to create instance of Singleton class (Singleton user)
	public static UserHolder getInstance() {
		if (single_instance == null)
			single_instance = new UserHolder();

		return single_instance;
	}

	public User getCurrentUser() {
		return CurrentUser;
	}

	public void setCurrentUser(User currentUser) {
		CurrentUser = currentUser;
	}

	public String getCookie() {
		return cookie;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
}
