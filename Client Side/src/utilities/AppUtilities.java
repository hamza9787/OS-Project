package utilities;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class AppUtilities {

	public Optional<ButtonType> showAlert(Alert.AlertType type, String title, String headerText) {
		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		Optional<ButtonType> buttonType = alert.showAndWait();
		return buttonType;
	}

	private static AppUtilities single_instance = null;

	// private constructor restricted to this class itself
	private AppUtilities() {
	}

	// static method to create instance of Singleton class
	public static AppUtilities getInstance() {
		if (single_instance == null)
			single_instance = new AppUtilities();
		return single_instance;
	}

}
