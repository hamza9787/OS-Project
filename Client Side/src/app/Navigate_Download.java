package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Navigate_Download extends Application {
	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/view/navigate_download_screen.fxml"));
		stage.setScene(new Scene(root));
		stage.setTitle("Navigate and Download");
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}