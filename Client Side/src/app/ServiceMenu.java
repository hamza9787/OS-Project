package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ServiceMenu extends Application{
	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/view/services_menu_screen.fxml"));
		stage.setScene(new Scene(root, 600, 400));
		stage.setTitle("Services Menu");
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
