package comparator;

import java.util.Comparator;
import model.Task;

public class TaskComparator implements Comparator<Task> {

	// the nice value will be updated whenever the task is queued depending on bt
	@Override
	public int compare(Task t1, Task t2) {
		if (t1.getPriority() > t2.getPriority())
			return 1;
		else if (t2.getPriority() > t1.getPriority())
			return -1;
		else {
			if (t1.updateNiceValue() > t2.updateNiceValue())
				return 1;

			else if (t1.updateNiceValue() > t2.updateNiceValue())
				return -1;

			else // equal priority and nice value
				return 0;
		}
	}

}
