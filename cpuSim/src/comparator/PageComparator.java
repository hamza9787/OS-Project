package comparator;

import java.util.Comparator;
import model.Page;

public class PageComparator implements Comparator<Page> {

	
	@Override
	public int compare(Page p1, Page p2) {
		if (p1.getTime() > p2.getTime())
			return 1;
		else if (p1.getTime() < p2.getTime())
			return -1;
		else {
			return 0;
		}
	}

}
