package model;

import java.util.PriorityQueue;

import comparator.TaskComparator;

public class CPU {
	private int id;
	private boolean idle = true; // initially the CPU is idle
	private Task task;
	private PriorityQueue<Task> AQueue = new PriorityQueue<Task>(100, new TaskComparator());
	private PriorityQueue<Task> EQueue = new PriorityQueue<Task>(100, new TaskComparator());
	private double idle_time = 0;
	private double last_task = 0;

	public CPU() {
	}

	public void swapQueues() {
		PriorityQueue<Task> temp = this.AQueue;
		this.AQueue = this.EQueue;
		this.EQueue = temp;
	}

	public double getIdle_time() {
		return idle_time;
	}

	public void setIdle_time(double idle_time) {
		this.idle_time = idle_time;
	}

	public double getLast_task() {
		return last_task;
	}

	public void setLast_task(double last_task) {
		this.last_task = last_task;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isIdle() {
		return idle;
	}

	public void setIdle(boolean idle) {
		this.idle = idle;
	}

	public PriorityQueue<Task> getAQueue() {
		return AQueue;
	}

	public void setAQueue(PriorityQueue<Task> AQueue) {
		this.AQueue = AQueue;
	}

	public PriorityQueue<Task> getEQueue() {
		return EQueue;
	}

	public void setEQueue(PriorityQueue<Task> EQueue) {
		this.EQueue = EQueue;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

}