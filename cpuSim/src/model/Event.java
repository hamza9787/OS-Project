package model;

public class Event {
	private double time;
	private Task task;
	private int type = 0; // o: arrival, 1: departure
	private double creationTime = 0;

	public Event() {
	}

	public Event(double time, Task task, int type, double creationTime) {
		super();
		this.time = time;
		this.task = task;
		this.type = type;
		this.creationTime = creationTime;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double d) {
		this.time = d;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public double getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(double creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return " Time: " + time + ", task: " + task + ", type: " + type + "\n";
	}

}
