package model;

import java.util.*;

import comparator.PageComparator;

public class Memory {

	private static Memory instance;

	public Memory() {}

	public static int getPageFault(int[] pages, int capacity) {
		
		PriorityQueue<Page> pagesQueue = new PriorityQueue<Page>(pages.length, new PageComparator());
		ArrayList<Integer> logical_memory = new ArrayList<>(capacity);
		int pageFaults = 0;

		for (int i = 0; i < pages.length; i++) {
			if (logical_memory.contains(pages[i])) { // item found, update only
				
				updatePage(pages[i], i, pagesQueue);
				System.out.println("Page updated: " + pages[i]);
				
			} else { // item not found
				if (logical_memory.size() < capacity) { // memory not full

					pagesQueue.add(new Page(pages[i], i)); // add to the queue
					logical_memory.add(pages[i]); // add to the memory
					pageFaults++;

					System.out.println("Page " + pages[i] + " not found");
				} else {
					// replace the current index with the least recently used
					Page removed = pagesQueue.poll();
					logical_memory.remove(logical_memory.indexOf(removed.getId()));
					Page p = new Page(pages[i], i);
					pagesQueue.add(p);
					logical_memory.add(p.getId());

					pageFaults++;

					System.out.println("Page " + pages[i] + " replaced with " + removed.getId());
				}
			}
		}
		return pageFaults;
	}

	public static boolean contains(int page, PriorityQueue<Page> pagesQueue) {
		for (Page p : pagesQueue)
			if (p.getId() == page)
				return true;
		return false;
	}

	public static void updatePage(int page_id, int time, PriorityQueue<Page> pagesQueue) {
		Page ptemp = null;
		for (Page p : pagesQueue) {
			if (p.getId() == page_id)
				ptemp = p;
		}
		pagesQueue.remove(ptemp);
		pagesQueue.add(new Page(page_id, time));
	}

}
