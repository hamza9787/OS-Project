package model;

public class Page {
	
	private int id;
	private int size;
	private int time;
	
	

	public int getTime() {
		return time;
	}



	public void setTime(int time) {
		this.time = time;
	}



	public Page(int id, int time) {
		super();
		this.id = id;
		this.time = time;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getSize() {
		return size;
	}



	public void setSize(int size) {
		this.size = size;
	}



	public Page() {
		
	}

}
