package model;

import java.util.PriorityQueue;

import comparator.PageComparator;
import sim1.CreateArrivals;

public class Task {

	private int id;
	private int priority;
	private double arrival_time, burst_time, slice_time;
	private int cpu = -1; // id of the cpu the task is assigned to
	private int schedPolicy; // 0: SCHED_OTHER, 1: SCHED_FIFO, 2: SCHED_RR
	private double wait_time;
	private double enter_queue_time;
	private boolean firstWait = true;
	private double responseTime;
	
	private int[] pages;

	public Task() {
	}

	public Task(int id, int priority, double arrival_time, double burst_time, int schedPolicy, int[] pages) {
		super();
		this.id = id;
		this.priority = priority;
		this.arrival_time = CreateArrivals.round(arrival_time, 2);
		this.burst_time = CreateArrivals.round(burst_time, 2);
		this.cpu = -1;
		this.schedPolicy = schedPolicy;
		this.slice_time = CreateArrivals.round(getSlice_time(), 2);
		this.wait_time = 0;
		this.pages = pages;
		
	}

	public void printPages() {
		for (int p : pages)
			System.out.println(p);
	}

	public int[] getPages() {
		return pages;
	}

	public void setPages(int[] pages) {
		this.pages = pages;
	}

	public int updateNiceValue() {
		return (int) (0.3 * (burst_time - wait_time));
	}

	public boolean isFirstWait() {
		return firstWait;
	}

	public void setFirstWait(boolean firstWait) {
		this.firstWait = firstWait;
	}

	public double getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(double responseTime) {
		this.responseTime = responseTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public double getArrival_time() {
		return arrival_time;
	}

	public void setArrival_time(double arrival_time) {
		this.arrival_time = arrival_time;
	}

	public double getBurst_time() {
		return burst_time;
	}

	public void setBurst_time(double burst_time) {
		this.burst_time = burst_time;
	}

	public double getSlice_time() {

//		if(schedPolicy == 1) // SCHED_FIFO
//			return burst_time;
//		// the higher number has a higher priority
//		return ((190/141) * priority) + 10;
//		
//		// if the lower number has the higher priority, the value will be:
//		// return (200 - ((190/141) * priority));

		if (schedPolicy == 0) // SCHED_OTHER
			return 10;
		else if (schedPolicy == 1) // SCHED_FIFO
			return burst_time;
		else // SCHED_RR
			return 100;
	}

	public void setSlice_time(double slice_time) {
		this.slice_time = slice_time;
	}

	public int getCpu() {
		return cpu;
	}

	public void setCpu(int cpu) {
		this.cpu = cpu;
	}

	public int getSchedPolicy() {

//		if(priority == 0) // NRT
//			return 0; //  SCHED_OTHER (goes to expired queue)
//		else if (priority >= 100)
//			return 1;
//		else
//			return 2;

		if (priority >= 100) // NRT
			return 0; // SCHED_OTHER (goes to expired queue)
		else
			return schedPolicy;
	}

	public void setSchedPolicy(int schedPolicy) {
		this.schedPolicy = schedPolicy;
	}

	public double getWait_time() {
		return wait_time;
	}

	public void setWait_time(double wait_time) {
		this.wait_time = wait_time;
	}

	public double getEnter_queue_time() {
		return enter_queue_time;
	}

	public void setEnter_queue_time(double enter_queue_time) {
		this.enter_queue_time = enter_queue_time;
	}

	@Override
	public String toString() {
		return " Task: " + id + ", Burst_t: " + burst_time + ", Slice_t: " + slice_time + ", cpu: " + cpu + " ";
	}
}
