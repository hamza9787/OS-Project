package sim1;

import java.util.PriorityQueue;
import java.util.Scanner;

import comparator.EventComparator;
import logging.Logger;
import model.CPU;
import model.Event;
import model.Memory;
import model.Task;

public class Sim {

	private PriorityQueue<Event> eventCalendar = new PriorityQueue<Event>(10, new EventComparator());
	private double clock = 0.0;
	private CPU[] cpus;

	private double cpu_idle_time = 0;
	private boolean isPreemptive;
	private Logger log;
	private int num_of_tasks;
	private int num_of_RT_tasks;
	private int num_of_NRT_tasks;

	private double total_RT_burst_time = 0;
	private double total_RT_wait_time = 0;
	private double total_RT_response_time = 0;
	private double total_RT_delay_time = 0;

	private double total_NRT_burst_time = 0;
	private double total_NRT_wait_time = 0;
	private double total_NRT_response_time = 0;
	private double total_NRT_delay_time = 0;
	
	private int total_page_faults = 0;

	public Sim(int num_of_cpus, int num_of_tasks, int schedPolicy, int percentage, boolean isPreemptive) {
		this.isPreemptive = isPreemptive;
		this.num_of_tasks = num_of_tasks;
		// ############## Create the Logging file ##########
		log = new Logger();

		// ############## Create the tasks using the Create Arrivals class #############
		new CreateArrivals(eventCalendar, num_of_tasks, schedPolicy, percentage);
		
		for(Event e: eventCalendar) {
			if(e.getTask().getPriority() > 100)
				num_of_NRT_tasks++;
			else
				num_of_RT_tasks++;
		}

		// ################ Create the CPUs using the num_of_cpus given ################
		cpus = new CPU[num_of_cpus];
		for (int i = 0; i < num_of_cpus; i++)
			cpus[i] = new CPU();
		log.log(clock + ":\tCPUs created");

		// Start the simulator
		startSim();
	}

	private void startSim() {

		log.log(clock + ":\tSim started");

		Event event;
		Task task;

		while (!eventCalendar.isEmpty()) {

			// 1. Take the first event in the queue
			event = eventCalendar.poll();
			task = event.getTask();
			
			// 2. Get stats from task
			if (task.getPriority() > 100) // NRT
				total_NRT_burst_time += task.getBurst_time();
			else // RT
				total_RT_burst_time += task.getBurst_time();
			
			// 3. Update the clock to match the event time
			clock = CreateArrivals.round(event.getTime(), 2);

			System.out.println("XXXXXXXXXXXXXXXXXXXXX   Clock: " + clock + "   XXXXXXXXXXXXXXXXXXXXXX");

			// Case 1: Departure Event
			if (event.getType() == 1) {

				// 1. Set the CPU as idle (empty)
				cpus[task.getCpu()].setIdle(true);
				cpus[task.getCpu()].setTask(null);
				cpus[task.getCpu()].setLast_task(clock);

				// 2. Check for remaining burst time
				double time_left = task.getBurst_time() - task.getSlice_time();
				if (time_left > 0) {

					// 3. Update the time left for the task (burst time)
					task.setBurst_time(time_left);

					// 4. Add the event back to the queue depending on SCHED_POLICY
					if (task.getSchedPolicy() == 0)
						cpus[task.getCpu()].getEQueue().add(event.getTask());
					else
						cpus[task.getCpu()].getAQueue().add(event.getTask());
					task.setEnter_queue_time(clock);
					
					// 5. Log the departure event
					log.log(clock + ":\tDeparture of:" + task.getId() + " with burst time left: "
							+ CreateArrivals.round(time_left, 2));
				}

				// ########### Main stats #############
				else { // no remaining time
					if (task.getPriority() > 100) { // NRT
						total_NRT_wait_time += task.getWait_time();
						total_NRT_response_time += task.getResponseTime();
						total_NRT_delay_time += (clock - task.getArrival_time());
					} else { // RT
						total_RT_wait_time += task.getWait_time();
						total_RT_response_time += task.getResponseTime();
						total_RT_delay_time += (clock - task.getArrival_time());
					}

					// Log the final departure
					log.log(clock + ":\tTask " + task.getId() + " has finished at CPU " + task.getCpu()
							+ " completely");
				}

				// swap queues in the CPU if aQueue is empty
				if (cpus[task.getCpu()].getAQueue().isEmpty() && !cpus[task.getCpu()].getEQueue().isEmpty())
					cpus[task.getCpu()].swapQueues();
			}
			
			// Case 2: Arrival Event
			if (event.getType() == 0) {
				
				task.setEnter_queue_time(clock);

				// 1. Get the emptiest CPU
				int cpu_id = nextCPU();
				
				// 2. Assign the task to the CPU and add it to the queue
				task.setCpu(cpu_id);
				cpus[cpu_id].getAQueue().add(task);
				
				log.log(clock + ":\tArrival of: Task " + task.getId() + " with priority: " + task.getPriority()
						+ " and bt: " + task.getBurst_time() + " to CPU " + cpu_id);
				
				// Preemptive
				Task interrupted;
				if (!cpus[cpu_id].isIdle() && isPreemptive) {
					if (higher_priority(task, cpus[cpu_id].getTask())) {
						
						// delete the departure event
						interrupted = interrupt_and_return_task(cpu_id);
						
						// add it back to the queue
						if (task.getPriority() > 100) { // NRT
							cpus[cpu_id].getEQueue().add(interrupted);
						} else { // RT
							cpus[cpu_id].getAQueue().add(interrupted);
						}
						
						// keep stats accurate
						cpus[task.getCpu()].setLast_task(clock);
						
						// log the interrupt
						log.log(clock + ":\tTask " + interrupted.getId() + " interrupted with "
								+ CreateArrivals.round(interrupted.getBurst_time(), 2) + " bt left");
					}
				}
			}

			// Servicing the task to the CPU
			if (cpus[task.getCpu()].isIdle() && !cpus[task.getCpu()].getAQueue().isEmpty()) {

				CPU this_cpu = cpus[task.getCpu()];

				// 1. Set the CPU to busy (Not Idle)
				this_cpu.setIdle(false);

				// 2. Get the next task in queue and assign it to the CPU
				Task service_task = this_cpu.getAQueue().poll();
				this_cpu.setTask(service_task);

				// 3. Update stats
				service_task.setWait_time(service_task.getWait_time() + (clock - service_task.getEnter_queue_time()));

				// 3.1 Stats for response time
				if (service_task.isFirstWait()) {
					service_task.setResponseTime(service_task.getWait_time());
					service_task.setFirstWait(false);
					
					if(service_task.getPriority() > 100)
						total_NRT_response_time += service_task.getResponseTime();
					else
						total_RT_response_time += service_task.getResponseTime();
				}

				// 3.2 Some more stats
				this_cpu.setIdle_time(this_cpu.getIdle_time() + (clock - this_cpu.getLast_task()));

				// 4. Create the departure event
				double eventTime = Math.min(service_task.getBurst_time(), service_task.getSlice_time()) + clock;
				Event newEvent = new Event(eventTime, service_task, 1, clock);

				// 5. Add the event to the calendar to be sorted based on time
				eventCalendar.add(newEvent);
				
				// 6. Memory management
				int pages[] = service_task.getPages();
				System.out.println("Pages: " + pages.length);
				for(int i: pages)
					System.out.println(i);
				
				int pageFaults = Memory.getPageFault(pages, (int) Math.ceil(pages.length * 0.25));
				total_page_faults += pageFaults;
				log.log(clock + ":\tPage faults: " + pageFaults + " for data of size: " + pages.length);
				
				// 7. Log the Service
				log.log(clock + ":\tService of: Task " + service_task.getId() + " to CPU " + task.getCpu());
				log.log(clock + ":\tNew Departure Event created at: " + newEvent.getTime());
			}
		}

		// calculate all statistics required
		log.log("##########  RT statistics  #########");
		log.log("Average task wait time: " + (total_RT_wait_time / num_of_RT_tasks));
		log.log("Average task response time: " + (total_RT_response_time / num_of_RT_tasks));
		log.log("Average task delay time: " + (total_RT_delay_time / num_of_RT_tasks));
		
		log.log("##########  NRT statistics  #########");
		log.log("Average task wait time: " + (total_NRT_wait_time / num_of_NRT_tasks));
		log.log("Average task response time: " + (total_NRT_response_time / num_of_NRT_tasks));
		log.log("Average task delay time: " + (total_NRT_delay_time / num_of_NRT_tasks));
		
		log.log("##########  CPU statistics  #########");
		
		for (int i = 0; i < cpus.length; i++) {
			if (cpus[i].getIdle_time() == 0) {
				cpu_idle_time += clock;
				log.log("CPU " + i + " utilization time: 0, idle time: " + clock);
			} else {
				cpu_idle_time += cpus[i].getIdle_time();
				log.log("CPU " + i + " utilization time: " + (clock - cpus[i].getIdle_time()) + ", idle time: " + cpus[i].getIdle_time());
			}
		}
		log.log("Total CPU idle time: " + cpu_idle_time);
		log.log("Average CPU idle time: " + (cpu_idle_time / cpus.length));
		log.log("Throughput: " + ((total_RT_burst_time + total_NRT_burst_time) / clock));

	}

	public int nextCPU() {
		/* This function returns the CPU with the least total load */
		int cpuID = 0;
		double minWeight = 9999;
		for (int i = 0; i < cpus.length - 1; i++) {

			if (cpus[i].isIdle()) {
				System.out.println("CPU " + i + " is idle");
				System.out.println("CPU chosen: " + i);
				return i;
			}
			double thisCpuWeight = getCpuWeight(i);
			if (thisCpuWeight < minWeight) {
				minWeight = thisCpuWeight;
				cpuID = i;
				System.out.println("Choosen CPU Updates to " + cpuID);
			}
			System.out.println("CPU chosen: " + cpuID);
		}
//		log.log(clock + ":\tCPU chosen: CPU " + cpuID);
		return cpuID;
	}

	public double getCpuWeight(int cpuID) {
		/* This function returns the load on a given CPU */
		double acc = 0;

		for (Task t : cpus[cpuID].getAQueue()) {
			acc += t.getBurst_time();
		}
		System.out.println("CPU: " + cpuID + " Queue : " + acc + " Task inside CPU: " + eventInCPUTime(cpuID)
				+ " Total wait: " + (acc + eventInCPUTime(cpuID)));
		return acc + eventInCPUTime(cpuID);
	}

	public double eventInCPUTime(int cpuID) {
		/* This function returns the load of the task currently inside a CPU */
		Task t = cpus[cpuID].getTask();
		if (t != null)
			return t.getBurst_time();
		return 0;
		// leftTime = t.getBurst_time() - (clock - t.getArrival_time());
	}

	public Task interrupt_and_return_task(int cpu) {
		/*
		 * This function is used for the preemptive part It stops the task inside the
		 * CPU and calculates the remaining burst time Then queues it back to the CPU
		 * 
		 * The object that interrupted must have a higher priority
		 */
		Task task = null;
		if (!cpus[cpu].isIdle()) {

			// 1. Get the departure event
			Event e = get_departure_event_from_calendar(cpu);
			if (e != null) {
				task = e.getTask();

				// 2. Calculate the time elapsed
				double time_elapsed = clock - e.getCreationTime();
				System.out.println("Time elapsed: " + time_elapsed);
				System.out.println("Time left: " + (task.getBurst_time() - time_elapsed));

				// 3. Update the burst time
				task.setBurst_time(task.getBurst_time() - time_elapsed);
			}
			cpus[cpu].setIdle(true);
		}
		return task;

	}

	public Event get_departure_event_from_calendar(int cpu) {
		/* This function returns the event of the task inside the CPU */
		if (!cpus[cpu].isIdle()) {
			for (Event e : eventCalendar) {
				if (e.getType() == 1 && e.getTask().getCpu() == cpu) {
					eventCalendar.remove(e);
					System.out.println("Departure Event deleted: " + e);
					return e;
				}
			}
		}
		return null;
	}

	public boolean higher_priority(Task t1, Task t2) {
		/*
		 * This function compares between the priorities of 2 tasks Used in preemptive
		 * to know if the task needs to be interrupted or not
		 */

		if (t1.getPriority() < t2.getPriority()) {
			System.out.println("Task " + t1.getId() + " has higher priority.");
			return true;

		} else {
			System.out.println("Task " + t2.getId() + " has higher priority.");
			return false;
		}
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int power, num_tasks, sched, percentage;
		String preemptive;
		do {
			System.out.print("***** CPUs *****\nNumber of CPUs : 2^");
			power = sc.nextInt();
			if (power > 5)
				System.out.println("Note: Max CPUs: 32 (2^5) only, please try again");
		} while (power > 5);

		do {
			System.out.print("***** Size *****\nNumber of tasks: ");
			num_tasks = sc.nextInt();
		} while (num_tasks < 0 || num_tasks > 100);

		do {
			System.out.print("***** Scheduling Policy *****\n1. SCHED_FIFO\n2. SCHED_RR\nPolicy chosen: ");
			sched = sc.nextInt();
		} while (sched < 1 || sched > 2);

		do {
			System.out.print("***** Percentage of NRT tasks *****\n0 -- 100\nPercentage of NRT tasks: ");
			percentage = sc.nextInt();
		} while (percentage < 0 || percentage > 100);

		sc.nextLine();
		do {
			System.out.print("***** Preemptive (Y/N) ? *****");
			preemptive = sc.nextLine();

		} while (!preemptive.equalsIgnoreCase("y") && !preemptive.equalsIgnoreCase("n"));

		boolean isPreemptive = ((preemptive.equalsIgnoreCase("y")) ? true : false);
		int num_of_cpus = (int) Math.pow(2, power);
		System.out.println("Num of CPUs: " + num_of_cpus);
		new Sim(num_of_cpus, num_tasks, sched, percentage, isPreemptive);

		sc.close();
	}
}
