package sim1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.PriorityQueue;

import model.Event;
import model.Memory;
import model.Task;

public class CreateArrivals {

	private final static double MIN_BURST_TIME = 10;
	private final static double MAX_BURST_TIME = 200;
	private final static int RT_RANGE = 99;
	private final static int NRT_RANGE = 141;
	private final static int TIME_RANGE = 30;
	
	
	public CreateArrivals(PriorityQueue<Event> ec, int size, int schedPolicy, int percentage) {
		Event event;
		double arrival_time = round(0, 2);
		int priority;

		for (int i = 0; i < size; i++) {
			int id = i;
			double burst_time = generateRandom(MIN_BURST_TIME, MAX_BURST_TIME);
			arrival_time += generateRandom(0, TIME_RANGE);
			int randomizer = (int) generateRandom(0, 100);
			if (randomizer < percentage)
				priority = (int) generateRandom(RT_RANGE + 1, NRT_RANGE);
			else
				priority = (int) generateRandom(0, RT_RANGE);
			int sched_policy = ((priority > 99) ? 0 : schedPolicy);

			int pages_size = (int) generateRandom(10, 40);
			int[] pages = new int[pages_size];
			for (int j = 0; j < pages_size; j++) {
				pages[j] = (int) generateRandom(0, pages_size - 1);
			}

			event = new Event();
			event.setCreationTime(round(0, 2));
			event.setTime(arrival_time);
			event.setType(0);
			event.setTask(new Task(id, priority, arrival_time, burst_time, sched_policy, pages));
			ec.add(event);
		}
	}

	public double generateRandom(double min, double max) {
		return round((Math.random() * (max - min)) + min, 2);
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
