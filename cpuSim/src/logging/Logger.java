package logging;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {
	
	private FileWriter fw;
	private BufferedWriter bw;
	
	public Logger() {}
	
	public void log(String status) {
		try {
			fw = new FileWriter("report.txt", true);
			bw = new BufferedWriter(fw);
			bw.write(status);
			bw.newLine();
			bw.newLine();
			bw.close();
			
			System.out.println("log ==> " + status);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
