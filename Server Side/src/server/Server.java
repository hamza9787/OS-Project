package server;

import java.net.*;

import services.*;
import java.io.*;

public class Server extends Thread { // multiple services each is a thread on different port
	ServerSocket server;
	Socket client;
	int port;
	LogInterface serverLog = new Log();

	public Server(int port) {
		this.port = port;
	}

	public void run() {
		try {
			server = new ServerSocket(port);
			while (true) {
				client = server.accept();
				switch (port) {
				case 4000:
					new Login_Server_Service(client, serverLog);
					break;
				case 4001:
					new Upload_File_Service(client, serverLog);
					break;
				case 4002:
					new Get_Path_Childs_Servics(client, serverLog);
					break;
				case 4003:
					new Reset_Password_Service(client, serverLog);
					break;
				case 4004:
					new Download_File_Service(client, serverLog);
					break;
				default:
					System.exit(0);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Server(4000).start();
		new Server(4001).start();
		new Server(4002).start();
		new Server(4003).start();
		new Server(4004).start();
	}
}
