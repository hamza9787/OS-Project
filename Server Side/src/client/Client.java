package client;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;

import model.User;

public abstract class Client {
	private static Socket server;
	private static Formatter toServer;
	private static Scanner fromServer;

	public static String loginService(String userName, String password) {
		try {
			server = new Socket("localhost", 4000);
			fromServer = new Scanner(server.getInputStream());
			toServer = new Formatter(server.getOutputStream());

			// send to server
			toServer.format("%s\n", userName);
			toServer.flush();
			toServer.format("%s\n", password);
			toServer.flush();

			String response = fromServer.nextLine();
			if (response.equals("{")) { // if authentication was successful, then the response will be the user JSON
										// object, so I have to loop for all lines to take the whole JSON object.
				while (true) {
					String line = fromServer.nextLine();
					if (line.equals("}")) {
						response += line;
						break;
					}
					response += line;
				}
				// this to be convert in client side, here for testing
				Gson gson = new Gson();
				User user = gson.fromJson(response, User.class);
				System.out.println(user);
			} else { // means the response is only one line, so I am just displaying it here.
				System.out.println(response);
			}
			return response;
		} catch (IOException e) {
			e.printStackTrace();
			return e.toString();
		}
	}

	public static void uploadFiles(List<File> uploadFiles, int indexOfCourse) {
		try {
			for (File uploadFile : uploadFiles) {
				server = new Socket("localhost", 4001);
				OutputStream toNet = server.getOutputStream();
				InputStream fileStream = new FileInputStream(uploadFile);
				toNet.write((uploadFile.getName() + "|" + indexOfCourse + "|" + "project" + "/").getBytes());
				toNet.flush();
				byte[] fileBufferBytes = fileStream.readAllBytes();
				toNet.write(fileBufferBytes);
				toNet.flush();
				server.close();
				if (fileStream != null)
					fileStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<String> getPathChildes() {
		Formatter toServer = null;
		Scanner fromServer = null;
		ArrayList<String> childs = new ArrayList<String>();
		try {
			server = new Socket("localhost", 4002);
			fromServer = new Scanner(server.getInputStream());
			toServer = new Formatter(server.getOutputStream());

			toServer.format("%s\n", "/home/abdulla/Desktop");
			toServer.flush();

			String responseLine = "";
			while (true) {
				responseLine = fromServer.nextLine();
				if (responseLine.equals("") || responseLine.isBlank() || responseLine.isEmpty())
					break;
				else
					childs.add(responseLine);
				System.out.println("Response: " + responseLine);
			}
			System.out.println(childs.toString());
		} catch (IOException e) {
			// TODO: handle exception
		}
		return childs;
	}

	public static String resetPassword(String email, String password1, String password2) {
		Formatter toServer = null;
		Scanner fromServer = null;
		String response = "";
		try {
			server = new Socket("localhost", 4003);
			fromServer = new Scanner(server.getInputStream());
			toServer = new Formatter(server.getOutputStream());

			toServer.format("%s\n", email);
			toServer.flush();
			toServer.format("%s\n", password1);
			toServer.flush();
			toServer.format("%s\n", password2);
			toServer.flush();

			response = fromServer.nextLine();
			System.out.println("Response: " + response);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (toServer != null)
					toServer.close();
				if (fromServer != null)
					fromServer.close();
				if (server != null)
					server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

	public static void main(String[] a) {
		// for testing Login
		String name = "Faisal";
		String password = "123456";
		System.out.println(loginService(name, password));

		// for testing uploading
		File f = new File("/home/abdulla/Desktop/program.c");
		//File f1 = new File("/home/abdulla/Desktop/Phase I.zip");
		List<File> uploadFiles = new ArrayList<File>();
		uploadFiles.add(f);
		//uploadFiles.add(f1);
		uploadFiles(uploadFiles, 1);

		// for testing for getting path child
		getPathChildes();

		// for testing reset password
		resetPassword("abdulla@email.com", "123", "123");
	}
}
