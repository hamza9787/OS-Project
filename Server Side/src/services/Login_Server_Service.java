package services;

import java.io.IOException;
import java.net.Socket;
import java.util.Formatter;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.User;
import utilities.AppUtilities;
import utilities.UserHolder;

public class Login_Server_Service extends Thread {
	private Socket client;
	private Scanner fromClient;
	private Formatter toClient;
	private User users[] = AppUtilities.readUsers();
	private String userToSend = "";
	String cookie = null;
	private LogInterface Log;

	public Login_Server_Service(Socket client, LogInterface b) {
		this.client = client;
		this.Log = b;
		this.run();
	}

	public void run() { // any code here is threaded It can be lengthy in time
		try {
			fromClient = new Scanner(client.getInputStream());
			toClient = new Formatter(client.getOutputStream());
			String username = fromClient.nextLine();
			String password = fromClient.nextLine();

			int login = login(username, password);

			switch (login) {
			case -1:
				toClient.format("%s\n", "You are out of attempts");
				Log.createLogBuffer("You are out of attempts", username);
				break;
			case 0:
				toClient.format("%s\n", "Incorrect Password!");
				Log.createLogBuffer("Incorrect Password!", username);
				break;
			case 1:
				toClient.format("%s\n", userToSend + "\n" + cookie);
				Log.createLogBuffer("Succes Login", username);
				Log.createLogBuffer("Current cnumber of onnected Users = " + UserHolder.getInstance().getNumberOfCurrentUsers(),
						"Server: ");
				break;
			case 2:
				toClient.format("%s\n", "You are not a member, Please register first!");
				Log.createLogBuffer("You are not a member, Please register first!", username);
				break;
			default:
				throw new IllegalArgumentException("Unexpected value: " + login);
			}

			toClient.flush();
		} catch (IOException e) {
			e.printStackTrace();
			Log.createLogBuffer(e.toString(), "username is invalied");
		} finally {
			try {
				if (client != null)
					client.close();
				if (fromClient != null)
					fromClient.close();
				if (toClient != null)
					toClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public int login(String userName, String password) {
		// -1 Out of attempts
		// 0 Wrong password
		// 1 Successfully logged in (will send User object as JSON string)
		// 2 not a user
		boolean userFound = false;
		int status = 0;
		if (userName.isEmpty() || userName.equals("") || password.isEmpty() || password.equals(""))
			return status;
		for (User user : users) {
			if (user.getUserName().equalsIgnoreCase(userName)) { // user is founded
				userFound = true;
				if (user.getAttempts() > 0) { // not exceeded attempts numbers
					if (!user.getPassword().equals(password)) { // wrong password
						user.decrementAttempts();
						AppUtilities.saveUsers(users);
					} else { // correct password
						status = 1;
						// set this user as current user in a singleton object,
						// TODO should also be added to the server log file
						cookie = AppUtilities.getRandomStringKey();
						UserHolder.getInstance().cachUser(user, cookie);
						// Stringy the user object to be set as a response
						Gson gson = new GsonBuilder().setPrettyPrinting().create();
						userToSend = gson.toJson(user);
						// also reset attempts
						user.reSetAttempts();
						AppUtilities.saveUsers(users);
					}
				} else { // already exceeded attempts numbers, no need to check password
					status = -1;
				}
				break; // when ever you did find the user, then no need to continue the loop
			}
		}
		if (!userFound) {
			status = 2;
		}
		return status;
	}
}
