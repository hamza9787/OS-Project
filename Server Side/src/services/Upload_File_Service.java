package services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import model.Course;
import model.User;
import utilities.UserHolder;

public class Upload_File_Service extends Thread {
	private Socket client;
	private LogInterface Log;

	public Upload_File_Service(Socket client, LogInterface b) {
		this.client = client;
		this.Log = b;
		this.run();
	}

	public void run() { // any code here is threaded It can be lengthy in time
		User user = null;
		String cookie = null;
		try {
			InputStream fromNet = client.getInputStream();
			byte[] streamBufferBytes = fromNet.readAllBytes();
			String file = new String(streamBufferBytes);
			int indexOfDelimiter = file.indexOf('/');
			// fileName|0|Assignment/fileContent
			// first cut at /
			// then split first part by |
			String fileNameAndPathInfo = file.substring(0, indexOfDelimiter);
			String[] fileNameAndPathInfoSeparated = fileNameAndPathInfo.split("[|]");
			System.out.println("ALL Submission File Info: " + fileNameAndPathInfo);
			cookie = fileNameAndPathInfoSeparated[0];
			if (UserHolder.getInstance().isAuthentecatedUser(cookie) != null)
				user = UserHolder.getInstance().isAuthentecatedUser(cookie);
//			else {
//				// toClient.format("%s\n", "Not Authenticated Request");
//				// toClient.flush();
//			return;
//		}
			String srcFileName = fileNameAndPathInfoSeparated[1];
			String courseIndex = fileNameAndPathInfoSeparated[2];
			String assignmentFolder = fileNameAndPathInfoSeparated[3];

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yy-MM-dd_HH:mm");
			LocalDateTime now = LocalDateTime.now();
			srcFileName = "Submit[" + dtf.format(now) + "]" + srcFileName;

			Course c = user.getCourses().get(Integer.parseInt(courseIndex));
			String coursesPath = "./../phaseI_team_6/c/FileStructure/adminSystem/";
			String projectPath = coursesPath + c.course + "/" + c.instructor + "/" + c.team + "/" + assignmentFolder
					+ "/src/" + srcFileName;
			File submissionFileFullPath = new File(projectPath);
			System.out.println(submissionFileFullPath.toString());

			FileOutputStream fileStream = new FileOutputStream(submissionFileFullPath);
			fileStream.write(streamBufferBytes, indexOfDelimiter + 1, streamBufferBytes.length - indexOfDelimiter - 1);

			if (fileStream != null)
				fileStream.close();

			Log.createLogBuffer(user.getUserName(),
					"File is uploaded useing Upload_File_Service: " + submissionFileFullPath);
			// Triggering AutoGader
			if (srcFileName.contains(".c")) { // make sure it is a C program
				try {
					String autoGraderPath = "./../phaseI_team_6/script/AutoGrader/autoGrader.sh";
					String srcPath = coursesPath + c.course + "/" + c.instructor + "/" + c.team + "/" + assignmentFolder
							+ "/src/";
					Process p = new ProcessBuilder(autoGraderPath, srcPath, srcFileName).start();
					Log.createLogBuffer("File is passed to autograder: " + submissionFileFullPath, user.getUserName());
					System.out.println(p);
				} catch (IOException e) {
					e.printStackTrace();
					Log.createLogBuffer("Error occurs: " + e.toString(), user.getUserName());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			Log.createLogBuffer("Error occurs: " + e.toString(), user.getUserName());
		} finally {
			try {
				if (client != null)
					client.close();
			} catch (IOException e) {
				Log.createLogBuffer("Error occurs: " + e.toString(), user.getUserName());
				e.printStackTrace();
			}
		}
	}

}