package services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;

import model.User;
import utilities.UserHolder;

public class Get_Path_Childs_Servics extends Thread {
	private Socket client;
	private Scanner fromClient;
	private Formatter toClient;
	private LogInterface Log;

	public Get_Path_Childs_Servics(Socket client, LogInterface b) {
		this.client = client;
		this.Log = b;
		this.run();
	}

	public void run() {
		User user = null;
		String cookie = null;
		try {
			fromClient = new Scanner(client.getInputStream());
			toClient = new Formatter(client.getOutputStream());

			cookie = fromClient.nextLine();
			if (UserHolder.getInstance().isAuthentecatedUser(cookie) != null)
				user = UserHolder.getInstance().isAuthentecatedUser(cookie);
//			else {
//				// toClient.format("%s\n", "Not Authenticated Request");
//				// toClient.flush();
//				return;
//			}
			
			String coursesPath = "./../phaseI_team_6/c/FileStructure/adminSystem/";
			String path = fromClient.nextLine();

			// this var has value 0 or 1, 0 => uploading service is consuming this service,
			// 1 => navigate&download service is consuming this service
			String whoIsRequsting = path.substring(0, 1);
			path = path.substring(1, path.length());

			path = coursesPath + path;
			File folder = new File(path);
			File[] listOfFiles = folder.listFiles();

			String responseToClient = "";

			Log.createLogBuffer("get list of files in this Path: " + path, user.getUserName());

			if (whoIsRequsting.equals("1")) {
				for (File f : listOfFiles) {
					if (f.isFile())
						responseToClient += f.getName().toString();
					if (f.isDirectory())
						responseToClient += "/" + f.getName().toString();
					responseToClient += "\n";
				}
			} else if (whoIsRequsting.equals("0")) {
				for (File f : listOfFiles) { // this part to display only folders inside a course
					// So I will not send files when the request is only one char (index of course),
					// since the user will only upload in a directory
					if (f.isDirectory()) {
						File dueDateFile = new File(f + "/dueDate.txt");
						String dueDate = "";
						String nowDate = "";
						try {
							BufferedReader br = new BufferedReader(new FileReader(dueDateFile));
							dueDate = br.readLine();

							SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
							DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
							LocalDateTime now = LocalDateTime.now();
							nowDate = dtf.format(now);

							Date d1 = null;
							Date d2 = null;
							try {
								d1 = sdf.parse(nowDate);
								d2 = sdf.parse(dueDate);
							} catch (ParseException e) {
								e.printStackTrace();
							}

							long difference_In_Time = d2.getTime() - d1.getTime();
							long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;

							System.out.println(difference_In_Days);

							if (difference_In_Time > 0)
								responseToClient += f.getName().toString() + "   |\t( " + difference_In_Days
										+ " Days left )";
							else
								responseToClient += f.getName().toString() + "   |\tPassed due";

							responseToClient += "\n";

							br.close();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
							Log.createLogBuffer(e.toString(), user.getUserName());
						}
					}
				}
			}

			toClient.format("%s\n", responseToClient);
			toClient.flush();

		} catch (IOException e) {
			e.printStackTrace();
			Log.createLogBuffer(e.toString(), user.getUserName());
		} finally {
			if (client != null)
				try {
					client.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

}
