package services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

import model.User;
import utilities.UserHolder;

public class Download_File_Service extends Thread {
	private Socket client;
	private Scanner fromClient;
	private LogInterface Log;

	public Download_File_Service(Socket client, LogInterface b) {
		this.client = client;
		this.Log = b;
		this.run();
	}

	public void run() { // any code here is threaded It can be lengthy in time
		User user = null;
		String cookie = null;
		try {
			fromClient = new Scanner(client.getInputStream());
			String coursesPath = "./../phaseI_team_6/c/FileStructure/adminSystem/";

			cookie = fromClient.nextLine();
			if (UserHolder.getInstance().isAuthentecatedUser(cookie) != null)
				user = UserHolder.getInstance().isAuthentecatedUser(cookie);
//			else {
//				// toClient.format("%s\n", "Not Authenticated Request");
//				// toClient.flush();
//				return;
//			}

			String filePath = fromClient.nextLine();

			filePath = coursesPath + filePath;

			OutputStream toClient = client.getOutputStream();
			File requestedFile = new File(filePath);
			System.out.println(requestedFile);

			if (requestedFile.exists()) {
				InputStream fileStream = new FileInputStream(filePath);
				toClient.write(fileStream.readAllBytes());
				toClient.flush();
				toClient.close();

				if (fileStream != null)
					fileStream.close();
			}
			// creating log file
			String log1 = "attemps to download file: " + filePath;
			Log.createLogBuffer(log1, user.getUserName());
		} catch (IOException e) {
			e.printStackTrace();
			String log1 = "Error occurs: " + e.toString();
			Log.createLogBuffer(log1, user.getUserName());
		}
	}
}
