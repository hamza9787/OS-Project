package services;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import utilities.*;

public class Log implements LogInterface {
	private Logger logger;
	private FileHandler fileHandler;
	private ArrayList<String> temp = new ArrayList<>(1);
	private Object[] log;

	public Log() {
		log = new Object[1];
	}

	@Override
	public synchronized void createLogBuffer(String log, String username) {
		while (temp.size() != 0)
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		temp.add(username + ": " + log);
		notify();
		createLog();
	}

	@Override
	public synchronized void createLog() {
		while (temp.size() == 0)
			try {
				wait();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		try {
			File f = new File("logs.txt");
			if (!f.exists())
				f.createNewFile();

			fileHandler = new FileHandler("logs.txt", true);
			logger = Logger.getLogger("LoginServices");
			logger.addHandler(fileHandler);
			SimpleFormatter formatter = new SimpleFormatter();
			fileHandler.setFormatter(formatter);
			logger.info(temp.remove(0));
			fileHandler.close();
			notify();
		} catch (SecurityException e) {

			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}