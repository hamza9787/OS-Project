package services;

import java.io.IOException;
import java.net.Socket;
import java.util.Formatter;
import java.util.Scanner;

import model.User;
import utilities.AppUtilities;
import utilities.UserHolder;

public class Reset_Password_Service extends Thread {
	private Socket client;
	private Scanner fromClient;
	private Formatter toClient;
	private User users[] = AppUtilities.readUsers();
	private LogInterface Log;

	public Reset_Password_Service(Socket client, LogInterface b) {
		this.client = client;
		this.Log = b;
		this.run();
	}

	public void run() { // any code here is threaded It can be lengthy in time
		User user = null;
		String cookie = null;
		try {
			fromClient = new Scanner(client.getInputStream());
			toClient = new Formatter(client.getOutputStream());

			cookie = fromClient.nextLine();
			if (UserHolder.getInstance().isAuthentecatedUser(cookie) != null)
				user = UserHolder.getInstance().isAuthentecatedUser(cookie);
//			else {
//				// toClient.format("%s\n", "Not Authenticated Request");
//				// toClient.flush();
//				return;
//			}

			String email = fromClient.nextLine();
			String password1 = fromClient.nextLine();
			String password2 = fromClient.nextLine();

			boolean resetPassword = resetPassword(email, password1, password2);

			if (resetPassword) {
				toClient.format("%s\n", "Reset Password Successfully");
				Log.createLogBuffer("Changing password using Rest_Password service successfully: ", user.getUserName());
			} else {
				toClient.format("%s\n", "Reset Password Fail");
				Log.createLogBuffer("Changing passwording using Rest_Password service Unsuccfull: ",
						user.getUserName());
			}

			toClient.flush();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (client != null)
					client.close();
				if (fromClient != null)
					fromClient.close();
				if (toClient != null)
					toClient.close();
			} catch (IOException e) {

			}
		}
	}

	public boolean resetPassword(String email, String password1, String password2) {
		boolean passUpdated = false;
		System.out.println(email);
		for (User user : users) {
			if (user.getEmail().equalsIgnoreCase(email)) { // user is founded
				System.out.println(user.getEmail());
				passUpdated = true;
				user.setPassword(password1);
				user.reSetAttempts();
				AppUtilities.saveUsers(users);
				return passUpdated;
			}
		}
		return passUpdated;
	}
}
