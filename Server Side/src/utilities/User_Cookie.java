package utilities;

import model.User;

public class User_Cookie {

	private User user;
	private String cookie;

	public User_Cookie(User user, String cookie) {
		this.user = user;
		this.cookie = cookie;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCookie() {
		return cookie;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
}
