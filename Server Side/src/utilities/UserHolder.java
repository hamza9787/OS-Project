package utilities;

import java.util.ArrayList;

import model.User;

public class UserHolder {

	private static UserHolder single_instance = null;
	private ArrayList<User_Cookie> CurrentUsers = new ArrayList<User_Cookie>();

	// private constructor restricted to this class itself
	private UserHolder() {
	}

	// static method to create instance of Singleton class
	public static UserHolder getInstance() {
		if (single_instance == null)
			single_instance = new UserHolder();

		return single_instance;
	}

	public void cachUser(User user, String cookie) {
		CurrentUsers.add(new User_Cookie(user, cookie));
	}

	public User isAuthentecatedUser(String cookie) {
		User user = null;
		for (User_Cookie uc : CurrentUsers)
			if (uc.getCookie().equals(cookie)) {
				user = uc.getUser();
				break;
			}
		return user;
	}

	public int getNumberOfCurrentUsers() {
		return CurrentUsers.size();
	}
}
