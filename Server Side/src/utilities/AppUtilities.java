package utilities;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.User;

public abstract class AppUtilities {
	
	public static User[] readUsers() {
		Gson gson = new Gson();
		String usersString = "";
		try {
			usersString = Files.readString(Paths.get("users.json"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		User[] users = gson.fromJson(usersString, User[].class);

		return users;
	}

	public static void saveUsers(User[] users) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try {
			Files.writeString(Paths.get("users.json"), gson.toJson(users));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static String getRandomStringKey() {
		// create a string of uppercase and lowercase characters and numbers
		String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
		String numbers = "0123456789";

		// combine all strings
		String alphaNumeric = upperAlphabet + lowerAlphabet + numbers;

		// create random string builder
		StringBuilder sb = new StringBuilder();

		// create an object of Random class
		Random random = new Random();

		// specify length of random string
		int length = 10;

		for (int i = 0; i < length; i++) {

			// generate random index number
			int index = random.nextInt(alphaNumeric.length());

			// get character specified by index
			// from the string
			char randomChar = alphaNumeric.charAt(index);

			// append the character to string builder
			sb.append(randomChar);
		}

		String randomString = sb.toString();
		return randomString;

	}
}
